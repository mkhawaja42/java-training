public class Rover {

    private int x;
    private int y;
    // private enum directions{
    //     North, South, East, West;
    // }
    private int direction;
    private String[] directions = {"North", "East", "South", "West"};
    // private boolean isBusy;
    private StringBuilder nextSteps = new StringBuilder();
    
    public void receiveCommands(String cmd){
        // System.out.println(cmd);
        this.nextSteps.append(cmd);
        // takeNextStep();
    }

    public void takeNextStep(){
        if(nextSteps.length() > 0){
        char first = nextSteps.charAt(0);
        nextSteps.deleteCharAt(0);
        // if first char is distance to travel
        if(Character.isDigit(first)){
            // North +1
            if(direction == 0){
                this.setY(y+Integer.parseInt(String.valueOf(first)));
            } //East +1
            else if(direction == 1){
                this.setX(x+Integer.parseInt(String.valueOf(first)));
            }//South -1
            else if(direction == 2){
                this.setY(y-Integer.parseInt(String.valueOf(first)));
            }//West =1
            else if(direction == 3){
                this.setX(x-Integer.parseInt(String.valueOf(first)));
            }
        }
        else{ //if char is direction
            if(first == 'R'){
                this.direction = direction+1;
            }
            if(first == 'L'){
                this.direction = direction-1;
            }
        }
    }
    }

    // public void getPosition(){
    //     System.out.println("get pos");
    //     //System.out.println(this.getX() + " ," + this.getY());
    // }

    public String getDirection(){
        //overflow of direction
        if(this.direction == -1) direction = 3;
        if(this.direction == 4) direction = 0;
        return directions[direction];
    }
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isBusy() {
        return nextSteps.length() > 0;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    // public void setBusy(boolean isBusy) {
    //     this.isBusy = isBusy;
    // }

}